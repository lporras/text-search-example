# == Schema Information
#
# Table name: posts
#
#  id            :integer          not null, primary key
#  title         :string
#  body          :text
#  stripped_body :text
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

require 'rails_helper'

RSpec.describe Post, type: :model do
  it { should validate_presence_of(:body) }
  it { should validate_presence_of(:title) }

  let(:post) { Fabricate(:post) }
  let(:html_post) { Fabricate(:html_post) }

  describe "Before save" do

    context "when having body as plain text" do
      it "should set stripped_body equal to body" do
        expect(post.stripped_body).to eq(post.body)
      end
    end

    context "when having body has html content" do
      it "should set stripped_body with the extracted text of body" do
        expect(html_post.stripped_body).to eq(html_post.body.strip_tags)
      end
    end
  end

  describe "Scopes" do
    describe ".latest" do
      it "should return posts in descending order by created_at" do
        expect(Post.latest.order_values).to eq(["created_at desc"])
      end
    end
  end

  describe "#to_param" do
    it "should return id and title" do
      expect(post.to_param.split('-')[0]).to eq(post.id.to_s)
      expect(post.to_param).to include(post.title.parameterize)
    end
  end

  describe ".paginates_per" do
    it "should be 12" do
      expect(Post.page(1).limit_value).to eq(12)
    end
  end
end
