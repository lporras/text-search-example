require "rails_helper"

RSpec.feature "Edit Post", :type => :feature do

  given(:post) { Fabricate(:post) }

  scenario "User edit a post with valid title" do
    visit edit_post_path(post)
    fill_in "Title", with: "My New Post"
    click_button I18n.t('helpers.submit.update', model: Post.model_name.human)
    expect(page).to have_text(I18n.t('notice', scope: "flash.actions.update", resource_name: Post.model_name.human))
    expect(page).to have_css("article h1", text: "My New Post")
  end

  scenario "User edit a post with valid content" do
    visit edit_post_path(post)
    fill_in "Content", with: "This is the new content"
    click_button I18n.t('helpers.submit.update', model: Post.model_name.human)
    expect(page).to have_text(I18n.t('notice', scope: "flash.actions.update", resource_name: Post.model_name.human))
    expect(page).to have_css("article p", text: "This is the new content")
  end

  scenario "User edit a post with invalid title" do
    visit edit_post_path(post)
    fill_in "Title", with: ""
    click_button I18n.t('helpers.submit.update', model: Post.model_name.human)
    expect(page).to have_css(".help-block", text: I18n.t('errors.messages.blank'))
    expect(page).to have_text(I18n.t('alert', scope: "flash.actions.update", resource_name: Post.model_name.human))
  end

  scenario "User edit a post with invalid content" do
    visit edit_post_path(post)
    fill_in "Content", with: ""
    click_button I18n.t('helpers.submit.update', model: Post.model_name.human)
    expect(page).to have_css(".help-block", text: I18n.t('errors.messages.blank'))
    expect(page).to have_text(I18n.t('alert', scope: "flash.actions.update", resource_name: Post.model_name.human))
  end

end