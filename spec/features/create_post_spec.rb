require "rails_helper"

RSpec.feature "Create Post", :type => :feature do

  scenario "User creates a valid post" do
    visit new_post_path
    fill_in "Title", with: "My New Post"
    fill_in "Content", with: Faker::Lorem.paragraph(2)
    click_button I18n.t('helpers.submit.create', model: Post.model_name.human)
    expect(page).to have_text(I18n.t('notice', scope: "flash.actions.create", resource_name: Post.model_name.human))
  end

  scenario "User tries to create a post without title" do
    visit new_post_path
    fill_in "Content", with: Faker::Lorem.paragraph(2)
    click_button I18n.t('helpers.submit.create', model: Post.model_name.human)
    expect(page).to have_text(I18n.t('alert', scope: "flash.actions.create", resource_name: Post.model_name.human))
  end

  scenario "User tries to create a post without content" do
    visit new_post_path
    fill_in "Title", with: "My New Post"
    click_button I18n.t('helpers.submit.create', model: Post.model_name.human)
    expect(page).to have_text(I18n.t('alert', scope: "flash.actions.create", resource_name: Post.model_name.human))
  end

end