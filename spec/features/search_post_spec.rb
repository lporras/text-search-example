require "rails_helper"

RSpec.feature "Search Post", :type => :feature do

  given(:posts) { Fabricate.times(5, :post) }

  scenario "User searches an existence post by title" do
    visit posts_path
    fill_in "q_title_or_stripped_body_cont", with: posts.first.title
    click_button I18n.t('search')
    expect(page).to have_css("#search-text", text: posts.first.title)
  end

  scenario "User searches an existence post by content" do
    word = posts.first.body.slice 1..5
    visit posts_path
    fill_in "q_title_or_stripped_body_cont", with: word
    click_button I18n.t('search')
    expect(page).to have_css("#search-text", text: word)
  end

  scenario "User searches a non existence post" do
    visit posts_path
    fill_in "q_title_or_stripped_body_cont", with: "non existence post"
    click_button I18n.t('search')
    expect(page).to have_text(I18n.t('helpers.page_entries_info.one_page.display_entries.zero', entry_name: Post.model_name.human.downcase.pluralize(2)))
  end

end