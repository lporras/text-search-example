require "rails_helper"

RSpec.feature "Delete Post", :type => :feature do
  given(:post) { Fabricate(:post) }

  scenario "User deletes a post" do
    visit post_path(post)
    click_link I18n.t('.delete', scope: "posts.show")
    expect(page).to have_text(I18n.t('notice', scope: "flash.actions.destroy", resource_name: Post.model_name.human))
  end
end