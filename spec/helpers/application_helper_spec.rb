require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do

  describe "#active_class" do
    context "when given link is the current_page" do
      it "should return active" do
        allow(helper).to receive("current_page?").with(root_path).and_return(true)
        expect(helper.active_class(root_path)).to eq('active')
      end
    end
    context "when given link is not the current_page" do
      it "should return blank" do
        expect(helper.active_class(root_path)).to eq('')
      end
    end
  end
end