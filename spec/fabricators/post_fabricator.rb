# == Schema Information
#
# Table name: posts
#
#  id            :integer          not null, primary key
#  title         :string
#  body          :text
#  stripped_body :text
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

Fabricator(:post) do
  title   { Faker::Name.title }
  body    { Faker::Lorem.paragraph(2) }
end

Fabricator(:html_post, from: :post) do
  body    { "<p>#{Faker::Lorem.paragraph}</p><p>#{Faker::Lorem.paragraph}</p>"}
end
