require 'rails_helper'

RSpec.describe HomeController, type: :controller do

  describe "GET #index" do
    let(:posts) { Fabricate.times(6, :post) }

    it "should render template index" do
      get :index
      expect(response).to render_template(:index)
    end

    it "should assign to @posts latest 5 post" do
      posts
      get :index
      expect(assigns(:posts)).to eq(Post.latest.limit(5))
    end
  end

end
