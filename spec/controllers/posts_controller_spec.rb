require 'rails_helper'

RSpec.describe PostsController, type: :controller do

  let(:post_params) { Fabricate.attributes_for(:post) }
  let(:invalid_post_params) { Fabricate.attributes_for(:post, body: nil) }

  describe "GET new" do
    it "assigns @post a new Post" do
      get :new
      expect(assigns(:post)).to be_a_new(Post)
    end

    it "renders the new template" do
      get :new
      expect(response).to render_template(:new)
    end
  end

  describe "POST create" do
    context "with valid attributes" do

      it "should assign to @post the created Post" do
        post :create, post: post_params
        expect(assigns(:post)).not_to be_a_new(Post)
        expect(assigns(:post)).to be_valid
      end

      it "should store Post with given params" do
        post :create, post: post_params
        expect(assigns(:post).title).to eq(post_params[:title])
        expect(assigns(:post).body).to eq(post_params[:body])
      end

      it "should redirect to posts#index" do
        post :create, post: post_params
        expect(response).to redirect_to posts_path
      end

      it "should set a notice message" do
        post :create, post: post_params
        expect(flash[:notice]).to eq(I18n.t('notice', scope: "flash.actions.create", resource_name: Post.model_name.human))
      end
    end

    context "with invalid attributes" do

      it "should assign to @post a new Post" do
        post :create, post: invalid_post_params
        expect(assigns(:post)).to be_a_new(Post)
      end

      it "should assign to @post an invalid Post" do
        post :create, post: invalid_post_params
        expect(assigns(:post)).not_to be_valid
      end

      it "renders the new template" do
        post :create, post: invalid_post_params
        expect(response).to render_template(:new)
      end

      it "should set an alert message" do
        post :create, post: invalid_post_params
        expect(flash[:alert]).to eq(I18n.t('alert', scope: "flash.actions.create", resource_name: Post.model_name.human))
      end
    end
  end

  describe "GET show" do
    let(:post) { Fabricate(:post) }

    it "should assign to @post the given post" do
      get :show, id: post.to_param
      expect(assigns(:post)).to eq(post)
    end

    it "renders to show template" do
      get :show, id: post.to_param
      expect(response).to render_template(:show)
    end
  end

  describe "GET edit" do

    let(:post) { Fabricate(:post) }

    it "should assign to @post the given post" do
      get :edit, id: post.to_param
      expect(assigns(:post)).to eq(post)
    end

    it "renders to edit template" do
      get :edit, id: post.to_param
      expect(response).to render_template(:edit)
    end
  end

  describe "PUT update" do

    let(:post) { Fabricate(:post) }
    let(:title) { Faker::Name.title }
    let(:body) { Faker::Lorem.paragraph(2) }

    context "with valid attributes" do
      it "should assign to @post the given post" do
        put :update, id: post.to_param , post: {title: title}
        expect(assigns(:post)).to eq(post)
      end

      it "should set to @post the given title" do
        put :update, id: post.to_param , post: {title: title}
        expect(assigns(:post).title).to eq(title)
      end

      it "should set to @post the given body" do
        put :update, id: post.to_param , post: {body: body}
        expect(assigns(:post).body).to eq(body)
      end

      it "should save valid @post" do
        put :update, id: post.to_param, post: {title: title, body: body}
        expect(assigns(:post)).to be_valid
      end

      it "redirects to the post page" do
        put :update, id: post.to_param, post: {title: title, body: body}
        expect(response).to redirect_to post_path(assigns(:post))
      end

      it "should set a notice message" do
        put :update, id: post.to_param, post: {title: title, body: body}
        expect(flash[:notice]).to eq(I18n.t('notice', scope: "flash.actions.update", resource_name: Post.model_name.human))
      end

    end

    context "with invalid attributes" do
      it "should assign to @post the given post" do
        put :update, id: post.to_param , post: invalid_post_params
        expect(assigns(:post)).to eq(post)
      end

      it "should assign to @post the invalid attributes" do
        put :update, id: post.to_param, post: invalid_post_params
        expect(assigns(:post)).not_to be_valid
      end

      it "should render edit template" do
        put :update, id: post.to_param, post: invalid_post_params
        expect(response).to render_template(:edit)
      end

      it "should set an alert message" do
        put :update, id: post.to_param, post: invalid_post_params
        expect(flash[:alert]).to eq(I18n.t('alert', scope: "flash.actions.update", resource_name: Post.model_name.human))
      end
    end
  end

  describe "DELETE destroy" do
    let(:post) { Fabricate(:post) }

    context "when post was succesfully deleted" do
      it "should assign to @post the record to delete" do
        delete :destroy, id: post.to_param
        expect(assigns(:post)).to eq(post)
      end

      it "should remove post from database" do
        delete :destroy, id: post.to_param
        expect(Post.find_by(id: post.id)).to be_nil
      end

      it "should redirect to posts_path" do
        delete :destroy, id: post.to_param
        expect(response).to redirect_to posts_path
      end

      it "should set a notice message" do
        delete :destroy, id: post.to_param
        expect(flash[:notice]).to eq(I18n.t('notice', scope: "flash.actions.destroy", resource_name: Post.model_name.human))
      end
    end

    context "when post was not deleted" do
      it "should redirect to post page" do
        allow_any_instance_of(Post).to receive(:destroy).and_return(false)
        delete :destroy, id: post.to_param
        expect(response).to redirect_to post_path(post)
      end
    end
  end

  describe "GET index" do
    let(:posts) { Fabricate.times(20, :post) }

    context "when there are no posts" do
      it "should assign to @post an empty ActiveRecord Relation" do
        get :index
        expect(assigns(:posts)).to be_a(ActiveRecord::Relation)
        expect(assigns(:posts).length).to eq(0)
      end
    end

    context "searching for posts" do
      let(:q_title) { posts.first.title }
      let(:q_body) { posts.first.body.slice 1..5 }

      it "should search posts by title" do
        get :index, {q: {title_or_stripped_body_cont: q_title}}
        expect(assigns(:posts).first.title).to eq(q_title)
      end

      it "should search posts by body" do
        get :index, {q: {title_or_stripped_body_cont: q_body}}
        expect(assigns(:posts).first.body).to include(q_body)
      end
    end

    it "should paginate posts by 12" do
      posts
      get :index
      expect(assigns(:posts).total_count).to eq(20)
      expect(assigns(:posts).count).to eq(12)
      expect(assigns(:posts).total_pages).to eq(2)
    end

    it "should render index template" do
      posts
      get :index
      expect(response).to render_template(:index)
    end
  end
end
