window.App = {
    highlightSearch: function() {
        var $searchContainerElement, e, enew, newe, query, searchContainerId, text, textElementId;
        textElementId = 'q_title_or_stripped_body_cont';
        text = document.getElementById(textElementId).value;

        var highlightElementsWithGivenQuery = function (elements, query) {
            Array.prototype.forEach.call(elements, function (element) {
                var e = element.innerHTML;
                var enew = e.replace(/(<mark>|<\/mark>)/igm, '');
                element.innerHTML = enew;
                newe = enew.replace(query, '<mark>$1</mark>');
                element.innerHTML = newe;
            });
        };

        if (text !== '') {
            query = new RegExp('(' + text + ')', 'gim');
            var queryElements = document.getElementsByClassName("term-highlight");
            highlightElementsWithGivenQuery(queryElements, query);
        }
    }
}

$(window).on("page:before-unload", function(e) {
    if (CKEDITOR && CKEDITOR.instances) {
        for(name in CKEDITOR.instances) {
            if (CKEDITOR.instances.hasOwnProperty(name)) {
                CKEDITOR.instances[name].destroy(true);
            }
        }
    }
});
