# == Schema Information
#
# Table name: posts
#
#  id            :integer          not null, primary key
#  title         :string
#  body          :text
#  stripped_body :text
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Post < ActiveRecord::Base
  validates :body, presence: true
  validates :title, presence: true

  before_save :strip_body

  scope :latest, -> { order('created_at desc') }

  paginates_per 12

  def to_param
    [id, title.parameterize].join("-")
  end

  private

  def strip_body
    self.stripped_body = self.body.to_s.strip_tags
  end
end
