class PostsController < ApplicationController

  respond_to :html

  def show
    @post = Post.find(params[:id])
  end

  def index
    @search = Post.ransack(params[:q])
    @posts = @search.result.page(params[:page])
  end

  def new
    @post = Post.new
  end

  def create
    @post = Post.create(post_params)
    respond_with @post, location: -> { posts_path }
  end

  def edit
    @post = Post.find(params[:id])
  end

  def update
    @post = Post.find(params[:id])
    @post.update_attributes(post_params)
    respond_with @post
  end

  def destroy
    @post = Post.find(params[:id])
    @post.destroy
    respond_with @post
  end

  private

  def post_params
    params.require(:post).permit(:title, :body)
  end
end
