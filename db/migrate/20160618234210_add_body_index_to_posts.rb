class AddBodyIndexToPosts < ActiveRecord::Migration
  def change
    add_index :posts, :stripped_body
  end
end
